package com.agenda.bankapplication.view.services;

import com.agenda.bankapplication.view.model.statement.Statement;
import com.agenda.bankapplication.view.model.user.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ServicesInterface {

    @GET("statements/{id}")
    Call<Statement> listProfile(@Path("id") String id);

    @FormUrlEncoded
    @POST("login")
    Call<User> loginProfile (@Field("user") String login, @Field("password") String password);
}
