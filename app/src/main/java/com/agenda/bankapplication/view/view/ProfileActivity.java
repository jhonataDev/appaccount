package com.agenda.bankapplication.view.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.agenda.bankapplication.R;
import com.agenda.bankapplication.view.model.user.User;
import com.agenda.bankapplication.view.services.RetrofitInit;
import com.agenda.bankapplication.view.adapter.AdapterStatement;
import com.agenda.bankapplication.view.model.statement.Statement;
import com.agenda.bankapplication.view.services.ServicesInterface;

import java.text.NumberFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private TextView nome;
    private TextView conta;
    private TextView saldo;
    private Button btn_logout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initializeVars();
        setAction();
        serviceStatement();

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void initializeVars() {
        nome = findViewById(R.id.tv_name);
        conta = findViewById(R.id.tv_conta);
        saldo = findViewById(R.id.tv_saldo);
        btn_logout = findViewById(R.id.btn_logout);
    }

    private void setAction() {
        Bundle bundle = getIntent().getExtras();
        User user = (User) bundle.getSerializable("usuario");

        nome.setText(user.getUserAccount().getName());
        conta.setText( user.getUserAccount().getBankAccount() + " / " + user.getUserAccount().getAgency());
        String num = String.valueOf(user.getUserAccount().getBalance()).replace(".", "");
        String numFormat = NumberFormat.getCurrencyInstance().format(Double.parseDouble(num));
        saldo.setText(numFormat);
    }

    private void serviceStatement() {
        ServicesInterface services = RetrofitInit.getRetrofit().create(ServicesInterface.class);
        Call<Statement> getRepo = services.listProfile("1");

        getRepo.enqueue(new Callback<Statement>() {
            @Override
            public void onResponse(Call<Statement> call, Response<Statement> response) {
                if (response.isSuccessful()) {
                    Statement statement = response.body();

                    AdapterStatement adapterStatement = new AdapterStatement(getApplicationContext(), statement.getStatementList(), R.layout.recycler_view_item);
                    RecyclerView recyclerView = findViewById(R.id.recyclerView);
                    recyclerView.setLayoutManager(new LinearLayoutManager(ProfileActivity.this));
                    recyclerView.setAdapter(adapterStatement);
                }
            }

            @Override
            public void onFailure(Call<Statement> call, Throwable t) {

            }
        });
    }

    public void logout(){
        android.app.AlertDialog.Builder alert = new AlertDialog.Builder(ProfileActivity.this);
        alert.setTitle("Sair");
        alert.setMessage("Tem certeza que deseja sair da aplicação?");
        alert.setCancelable(false);
        alert.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        alert.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alert.show();
    }
}