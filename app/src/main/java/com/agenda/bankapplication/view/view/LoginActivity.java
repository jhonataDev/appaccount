package com.agenda.bankapplication.view.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.agenda.bankapplication.R;
import com.agenda.bankapplication.view.model.user.User;
import com.agenda.bankapplication.view.services.RetrofitInit;
import com.agenda.bankapplication.view.services.ServicesInterface;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Button btn_login;
    private EditText pass;
    private EditText login;
    private String validateMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeVars();

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceUser();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        login.setText("");
        pass.setText("");
    }

    private void initializeVars() {
        btn_login = findViewById(R.id.btn_login);
        pass = findViewById(R.id.et_pass);
        login = findViewById(R.id.et_login);
        validateMsg = "Sua senha deve ter pelo menos uma letra maiuscula, um caracter especial e um caracter alfanumérico";
    }

    private void serviceUser(){
        ServicesInterface services = RetrofitInit.getRetrofit().create(ServicesInterface.class);
        Call<User> getAccount = services.loginProfile("A", "1");

        getAccount.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (!isValidPassword(pass.getText().toString())) {
                    pass.setError(validateMsg);
                    pass.requestFocus();
                } else {
                    User user = response.body();
                    Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("usuario", user);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
            }
        });
    }

    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=*?])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();
    }
}