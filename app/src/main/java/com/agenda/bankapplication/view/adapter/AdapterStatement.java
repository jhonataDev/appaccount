package com.agenda.bankapplication.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agenda.bankapplication.R;
import com.agenda.bankapplication.view.model.statement.StatementList;

import java.text.NumberFormat;
import java.util.List;

public class AdapterStatement extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private Context context;
        private List<StatementList> list;
        private int resource;

        public AdapterStatement(Context context, List<StatementList> list, int resource) {
                this.context = context;
                this.list = list;
                this.resource = resource;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view =  LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.recycler_view_item, viewGroup, false);

                return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

                StatementList statement = list.get(position);
                ViewHolder mViewHolder = (ViewHolder) viewHolder;

                mViewHolder.titulo.setText(statement.getTitle());
                mViewHolder.fatura.setText(statement.getDesc());
                mViewHolder.data_fatura.setText(statement.getDate());

                String numFormat = NumberFormat.getCurrencyInstance().format(statement.getValue());
                mViewHolder.saldo_fatura.setText(numFormat);
        }

        @Override
        public int getItemCount() {
                return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

                private TextView fatura;
                private TextView data_fatura;
                private TextView saldo_fatura;
                private TextView titulo;

                public ViewHolder(@NonNull View itemView) {
                        super(itemView);

                        titulo = itemView.findViewById(R.id.tv_title);
                        fatura = itemView.findViewById(R.id.tv_fatura);
                        data_fatura = itemView.findViewById(R.id.tv_date_fatura);
                        saldo_fatura = itemView.findViewById(R.id.tv_valor_fatura);
                }
        }
}
